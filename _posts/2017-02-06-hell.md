---
layout: post
title:  the 9 circles of hell
date:   2017-02-03 10:10:26 -0800
comments: true
---

9. unbaptized infants / virtuous pagans / jimmy carter
8. gross bugs nobody really likes
7. millenials / youtube comments / digital marketing
6. people who order a whole weird pizza instead of half weird pizza half the kind their friends like
5. bill cosby / big pants jared / etc
4. every US president except jimmy carter
3. christians who picked the "wrong" denomination
2. goldman sachs
1. lucifer himself
0. people who take up 2 seats on the train
